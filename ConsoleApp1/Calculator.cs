﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Calculator
    {
        public long Multiply(int a, int b)
        {
            return a * b;
        }
        public long Add(int a, int b)
        {
            return a + b;
        }
        public long Subtract(int a, int b)
        {
            return a - b;
        }
    }
}
